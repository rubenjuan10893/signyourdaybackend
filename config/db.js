const mongoose = require("mongoose");
const dotenv = require("dotenv");
dotenv.config();

const connectDB = async () => {
  const connection = await mongoose.connect(process.env.URI_DDBB, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  });

  console.log(
    `MongoDB Connected: \x1b[32m%s\x1b[0m`,
    `${connection.connection.host}`
  );
};

module.exports = connectDB;
