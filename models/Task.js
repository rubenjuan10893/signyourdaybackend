// Para trabajar con mongoose y sus funciones hay que importarlo primero
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const taskState = ["Done", "In Progress"];
const taskStatus = ["Accepted", "Declined", "Pending"];

const TaskSchema = new Schema({
  title: { type: String, required: [true, "Title is Required"] },
  description: String,
  project: String,
  user: { type: Schema.Types.ObjectId, ref: "User" },
  status: {
    type: String,
    enum: taskStatus,
    default: taskStatus[taskStatus.length - 1],
  },
  state: { type: String, enum: taskState, default: taskState[1] },
  createdAt: { type: Date, default: new Date() },
});

module.exports = mongoose.model("Task", TaskSchema);
