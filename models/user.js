// Para trabajar con mongoose y sus funciones hay que importarlo primero
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const role = ["Admin", "User"];

const userSchema = new Schema({
  name: { type: String, required: [true, "Name is required"] },
  lastname: String,
  email: {
    type: String,
    unique: true,
    required: [true, "Email is required"],
  },
  role: { type: String, enum: role, default: "User" },
  avatar: { type: String, default: "http://www.gravatar.com/avatar/?d=mp" },
  password: {
    type: String,
    required: [true, "Password is required"],
  },
});

module.exports = mongoose.model("User", userSchema);
