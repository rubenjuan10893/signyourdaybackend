const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const signState = ["Accepted", "Pending", "Declined"];

const SignSchema = new Schema({
  date_start: { type: Date, default: null },
  date_end: { type: Date, default: null },
  state: { type: String, enum: signState, default: "Pending" },
  total_hours: String,
  estimated_hours: { type: Number, default: 8 },
  user: { type: Schema.Types.ObjectId, ref: "User" },
});

module.exports = mongoose.model("Sign", SignSchema);
