const Sign = require("../models/sign");

exports.start_sign = (req, res, next) => {
  const new_sign = {
    date_start: new Date(),
    date_end: null,
    user: req.body?.user,
  };

  const sign = new Sign({ ...new_sign });

  sign.save().then(() => {
    res.status(200).json({
      ok: true,
      msg: "El fichaje ha comenzado",
      sign: sign,
    });
  });
};

exports.end_sign = (req, res, next) => {
  const endSign = {
    date_end: req.body.date_end,
    total_hours: req.body.total_hours,
  };

  console.log(req.query);
  Sign.findOneAndUpdate({ _id: req.body.id }, { ...endSign }, { new: true })
    .populate("user", "name email")
    .then((theSign) => {
      console.log(theSign);
      if (theSign !== null) {
        res.status(200).json({
          ok: true,
          msg: "El fichaje ha finalizado",
          sign: theSign,
        });
      }
    });
};

exports.get_total_hours = (req, res, next) => {
  const id = req.params.id;
  Sign.find({ user: id })
    .populate("user")
    .then((signs) => {
      if (signs !== null) {
        let totalSeconds = 0;
        let totalMinutes = 0;
        let totalHours = 0;

        signs.map((sign) => {
          console.log(sign);
          if (sign.date_end !== null) {
            const timeSplit = sign.total_hours.split(":");
            totalSeconds += parseInt(timeSplit[timeSplit.length - 1]);
            totalMinutes += parseInt(timeSplit[timeSplit.length - 2]);
            totalHours += parseInt(timeSplit[timeSplit.length - 3]);
          }
        });

        while (totalSeconds > 60) {
          totalSeconds -= 60;
          totalMinutes++;
        }

        while (totalMinutes > 60) {
          totalMinutes -= 60;
          totalHours++;
        }

        console.log(totalHours, totalMinutes, totalSeconds);
        res.status(200).json({
          total_hours: totalHours,
          total_minutes: totalMinutes,
          total_seconds: totalSeconds,
        });
      }
    });
};

exports.update_sign_user = (req, res, next) => {
  const idSign = req.params.id;
  const signToUpdate = req.body;

  console.log(signToUpdate);

  Sign.findOneAndUpdate({ _id: idSign }, { ...signToUpdate }, { new: true })
    .then((the_sign) => {
      console.log(the_sign);
      res.status(200).json({
        the_sign,
      });
    })
    .catch(next);
};

exports.get_sign_user = async (req, res, next) => {
  const per_page = req.query.per_page > 0 ? parseInt(req.query.per_page) : 25;
  const page = req.query.page > 0 ? parseInt(req.query.page) : 0;
  const id = req.query.id;

  try {
    const [sign, sign_count] = await Promise.all([
      Sign.find({ user: id })
        .populate("user", "email")
        .limit(per_page)
        .skip(per_page * page)
        .sort({ date_start: -1 }),
      Sign.countDocuments({ user: id }),
    ]);

    res.status(200).json({
      sign,
      sign_count,
    });
  } catch (error) {
    next(error);
  }
};

exports.get_all_sign = async (req, res, next) => {
  try {
    const [signs, signs_count] = await Promise.all([
      Sign.find({}).populate("user"),
      Sign.countDocuments({}),
    ]);

    res.status(200).json({
      signs,
      signs_count,
    });
  } catch (error) {
    next(error);
  }
};
