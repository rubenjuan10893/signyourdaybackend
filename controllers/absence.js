const Absence = require("../models/Absence");

exports.create_absence = (req, res, next) => {
  const newAbsence = {
    title: req.body.title,
    description: req.body.description,
    date_absence: req.body?.date_absence,
    user: req.body.user,
  };

  const absence = new Absence({ ...newAbsence });
  absence
    .save()
    .then((the_absence) => {
      if (the_absence !== null) {
        res.status(200).json({
          the_absence,
        });
      }
    })
    .catch((error) => {
      res.status(400).json({
        error: {
          message: `No ha sido posible crear la ausencia ${error}`,
        },
      });
    });
};

exports.get_absences_list = async (req, res, next) => {
  const per_page = req.query.per_page > 0 ? parseInt(req.query.per_page) : 25;
  const page = req.query.page > 0 ? parseInt(req.query.page) : 0;

  try {
    const [absences, count_absences] = await Promise.all([
      Absence.find({})
        .populate("user")
        .limit(per_page)
        .skip(per_page * page)
        .sort({ date_absence: -1 }),
      Absence.countDocuments({}),
    ]);

    if (absences.length > 0) {
      res.status(200).json({
        absences,
        count_absences,
      });
    } else {
      res.status(404).json({
        error: {
          message: "No hay ausencias registradas",
        },
      });
    }
  } catch (error) {
    next(error);
  }
};

exports.get_user_absences = async (req, res, next) => {
  const id = req.params.id;
  const per_page = req.query.per_page > 0 ? parseInt(req.query.per_page) : 25;
  const page = req.query.page > 0 ? parseInt(req.query.page) : 0;

  try {
    const [absences, count_absences] = await Promise.all([
      Absence.find({ user: id })
        .populate("user", "name lastname avatar")
        .limit(per_page)
        .skip(per_page * page)
        .sort({ date_absence: -1 }),
      Absence.countDocuments({ user: id }),
    ]);

    if (absences.length > 0) {
      res.status(200).json({
        absences,
        count_absences,
      });
    } else {
      res.status(200).json({
        absences,
        count_absences,
        error: {
          message: "No hay ausencias registradas",
        },
      });
    }
  } catch (error) {
    console.log(error);
  }
};

exports.update_absence_user = async (req, res, next) => {
  const id = req.params.id;
  const updateAbsence = req.body;

  Absence.findByIdAndUpdate(id, { ...updateAbsence }, { new: true }).then(
    (the_absence) => {
      if (the_absence !== null) {
        res.status(200).json({
          updatedAbsence: the_absence,
        });
      } else {
        res.status(400).json({
          error: {
            message: "No ha sido posible actualizar la ausencia",
          },
        });
      }
    }
  );
};
