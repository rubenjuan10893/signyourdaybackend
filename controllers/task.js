const Task = require("../models/Task");

exports.create_task = (req, res, next) => {
  const new_task = {
    title: req.body.title,
    description: req.body.description,
    project: req.body.project,
    user: req.body.user,
  };

  const task = new Task({ ...new_task });

  task
    .save()
    .then((the_task) => {
      if (the_task !== null) {
        res.status(200).json({
          createdTask: the_task,
        });
      } else {
        res.status(400).json({
          error: {
            message: "Ha ocurrido un error al crear la tarea",
          },
        });
      }
    })
    .catch(next);
};

exports.get_task_list = async (req, res, next) => {
  const per_page = req.query.per_page > 0 ? parseInt(req.query.per_page) : 25;
  const page = req.query.page > 0 ? parseInt(req.query.page) : 0;

  const [tasks, count_tasks] = await Promise.all([
    Task.find({})
      .populate("user", "-password")
      .limit(per_page)
      .skip(per_page * page),
    Task.countDocuments({}),
  ]);

  if (tasks.length === 0) {
    res.status(404).json({
      error: {
        message: "No hay tareas registradas",
      },
    });
  } else {
    res.status(200).json({
      tasks,
      count_tasks,
    });
  }
};

exports.get_user_task = async (req, res, next) => {
  const per_page = req.query.per_page > 0 ? parseInt(req.query.per_page) : 25;
  const page = req.query.page > 0 ? parseInt(req.query.page) : 0;
  const id = req.params.id;

  const [tasks, count_tasks] = await Promise.all([
    Task.find({ user: id })
      .populate("user", "-password")
      .limit(per_page)
      .skip(per_page * page),
    Task.countDocuments({}),
  ]);

  if (tasks.length === 0) {
    res.status(404).json({
      error: {
        message: "No hay tareas registradas",
      },
    });
  } else {
    res.status(200).json({
      tasks,
      count_tasks,
    });
  }
};

exports.update_task = (req, res, next) => {
  const id = req.params.id;
  let status = "";
  let state = "";

  if (req.body.status === "Declined") {
    status = "Pending";
    state = "In Progress";
  }

  const updateTask = {
    ...req.body,
    status: status || req.body.status,
    state: state || req.body.state,
  };

  Task.findByIdAndUpdate(id, { ...updateTask }, { new: true }).then(
    (the_task) => {
      if (the_task !== null) {
        res.status(200).json({
          updatedTask: the_task,
        });
      } else {
        res.status(400).json({
          error: {
            message: "Ha ocurrido un error al actualizar la tarea",
          },
        });
      }
    }
  );
};
