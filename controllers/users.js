var User = require("../models/user");
const bcrypt = require("bcryptjs");

/* ***************************************** */
/* ****** OBTENER TODOS LOS USUARIOS ******* */
/* ***************************************** */

exports.getUsers = async (req, res, next) => {
  const [users, count_users] = await Promise.all([
    User.find({}),
    User.countDocuments({ role: "User" }),
  ]).catch(next);

  if (users === null) {
    res.status(404).json({
      error: {
        message: "No hay usuarios",
      },
    });
  } else {
    res.status(200).json({ users, count_users });
  }
};

// /* ************************************* */
// /* ****** CREAR UN NUEVO USUARIO ******* */
// /* ************************************* */

exports.create_user = (req, resp, next) => {
  if (req.body) {
    const name = req.body?.name;
    const email = req.body?.email;
    const password = req.body?.password;
    const role = req.body?.role;

    const newUser = {
      name: name,
      email: email,
      password: bcrypt.hashSync(password, 10),
      role: role,
    };

    User.findOne({ email: newUser.email }).then((found_user) => {
      if (found_user !== null) {
        resp.status(400).json({
          ok: false,
          user: null,
          errors: { message: "El email ya esta registrado" },
        });
      } else {
        const user = new User({ ...newUser });
        user
          .save()
          .then((the_user) => {
            resp.status(200).json({
              ok: true,
              user: { ...newUser },
            });
          })
          .catch(next);
      }
    });
  } else {
    resp.status(404).json({
      ok: false,
      user: null,
      errors: { message: "No se ha podido crear el usuario" },
    });
  }
};

exports.get_user = (req, res, next) => {
  const id = req.params.id;

  console.log(id);
  User.findById(id).then((the_user) => {
    if (the_user !== null) {
      res.status(200).json({
        user: the_user,
      });
    } else {
      res.status(400).json({
        error: {
          message: "No existe el usuario o ha sido borrado",
        },
      });
    }
  });
};

exports.update_user_get = (req, res, next) => {
  const id = req.params.id;

  User.findById(id).then((the_user) => {
    if (the_user === null) {
      res.status(400).json({
        error: {
          message: "No existe el usuario o ha sido borrado",
        },
      });
    } else {
      res.status(200).json({
        user: the_user,
      });
    }
  });
};

exports.update_user_post = (req, res, next) => {
  const id = req.params.id;
  const updateUser = {
    ...req.body,
  };

  console.log(updateUser);
  User.findByIdAndUpdate({ _id: id }, { ...updateUser }, { new: true }).then(
    (the_user_updated) => {
      if (the_user_updated === null) {
        res.status(400).json({
          error: {
            message: "No existe el usuario o ha sido borrado",
          },
        });
      } else {
        res.status(200).json({
          user: the_user_updated,
        });
      }
    }
  );
};
