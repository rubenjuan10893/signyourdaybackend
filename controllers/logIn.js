const User = require("../models/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const SEED = require("../config/constants").SEED;

/* ********************************************************* */
/* ****** AUTENTICACIÓN NORMAL MEDIANTE TOKEN PROPIO ******* */
/* ********************************************************* */

exports.login = (req, resp, next) => {
  const email = req.body.email;
  const passwd = req.body.password;

  console.log(email);
  console.log(passwd);

  User.findOne({ email: email }).then((user) => {
    if (!user) {
      return resp.status(400).json({
        ok: false,
        mensaje: "Credenciales incorrectas - email",
      });
    }

    if (!bcrypt.compareSync(passwd, user.password)) {
      return resp.status(400).json({
        ok: false,
        mensaje: "Credenciales incorrectas - password",
      });
    }

    // Si todo lo anterior está correcto, creamos el TOKEN
    var token = jwt.sign({ user: user }, SEED, {
      expiresIn: 14400,
    }); /* Expira en 4 horas */

    resp.status(200).json({
      ok: true,
      mensaje: "Login works!",
      //   usuario: user,
      token: token,
    });
  });
};
