const express = require("express");
const sign_controller = require("../controllers/sign");

const routes = express.Router();

routes.post("/start", sign_controller.start_sign);
routes.post("/end", sign_controller.end_sign);
routes.get("/all", sign_controller.get_all_sign);
routes.get("/total-hours/:id", sign_controller.get_total_hours);
routes.post("/:id/update", sign_controller.update_sign_user);
routes.get("/", sign_controller.get_sign_user);

module.exports = routes;
