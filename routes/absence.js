const express = require("express");
const absence_controller = require("../controllers/absence");

const routes = express.Router();

routes.post("/create", absence_controller.create_absence);
routes.post("/:id/update", absence_controller.update_absence_user);
routes.get("/:id", absence_controller.get_user_absences);
routes.get("/", absence_controller.get_absences_list);

module.exports = routes;
