const express = require("express");
const task_controller = require("../controllers/task");

const routes = express.Router();

routes.get("/", task_controller.get_task_list);
routes.get("/:id", task_controller.get_user_task);
routes.post("/create", task_controller.create_task);
routes.post("/:id/update", task_controller.update_task);

module.exports = routes;
