const express = require("express");
const login_controller = require("../controllers/logIn");

const routes = express.Router();

routes.post("/", login_controller.login);

module.exports = routes;
