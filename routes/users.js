const express = require("express");
const users_controller = require("../controllers/users");

const routes = express.Router();

routes.get("/", users_controller.getUsers);
routes.get("/:id", users_controller.get_user);
routes.get("/:id/update", users_controller.update_user_get);
routes.post("/:id/update", users_controller.update_user_post);
routes.post("/create", users_controller.create_user);

module.exports = routes;
