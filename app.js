const express = require("express");
const morgan = require("morgan");
const dotenv = require("dotenv");
const connectDB = require("./config/db");
const bodyParser = require("body-parser");

const path = require("path");
const cors = require("cors");

// routes
const users = require("./routes/users");
const login = require("./routes/login");
const sign = require("./routes/sign");
const absence = require("./routes/absence");
const task = require("./routes/task");

// Connect DB
connectDB();

// enable env vars
dotenv.config();

// Create App
const app = express();

app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

// Escuchar peticiones express
app.use("/login", login);
app.use("/users", users);
app.use("/absence", absence);
app.use("/task", task);
app.use("/sign", sign);

const port = process.env.SERVER_PORT || 5000;

const server = app.listen(port, () =>
  console.log(
    `Express server inizializado en el puerto ${port}: \x1b[32m%s\x1b[0m`,
    "online"
  )
);

process.on("unhandledRejection", (err, promise) => {
  console.log(`An error has occurred ${err.message}`);

  server.close(() => process.exit(1));
});
